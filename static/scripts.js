//function revealSpoilers() {
//	var s = document.getElementsByClassName('spoiler');
//
//	for (var i=0; i < s.length; i++)
//	  s[i].style.visibility = 'visible';
//}

var pona_old = "";
function toggle_sitelen_pona(value) {

	let elements_1 = document.querySelectorAll(".pona");

	for (let i=0; i < elements_1.length; i++) {
		if (value) elements_1[i].classList.add(value);
		if (pona_old) elements_1[i].classList.remove(pona_old);
	}
	
	pona_old = value;
}

let p1names = ["pimeja","laso","loje"];

function nasin_tenpo(dt)
{
  var p1 = parseInt(dt.getHours() / 8);
  var p2 = parseInt((dt.getHours() % 8) / 2);
  var p3 = parseInt(((dt.getHours() % 2) * 2) + ((dt.getMinutes() >= 30) ? 1 : 0));

  var tenpo = p1names[p1] + ":" + p2 + ":" + p3;

  return tenpo;
}

function internet_time(dt)
{
  return (((dt.getUTCHours() + 1) % 24) + dt.getUTCMinutes() / 60 + dt.getUTCSeconds() / 3600) * 1000 / 24;
}

function update_clock()
{
	var dt = new Date();
	var it = internet_time(dt);
	document.getElementById("itime-value").innerHTML = Math.floor(it);
	document.getElementById("itime-progress").style.width = (it - Math.floor(it)) * 100 + "%";
	var tp = nasin_tenpo(dt);
	document.getElementById("tenpo-value").innerHTML = tp;
}

function update_every_second()
{
	update_clock();
	var t = setTimeout(function() {update_every_second() }, 1000); //timer
}

update_every_second();
