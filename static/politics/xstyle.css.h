#define HASH(x) x

.header {
  background: #fefefe;
  color: #050403;
}

@media screen {
	
	.header_image {
		padding-top: 12.5%;
		background-image: url("/politics/news_header.gif");
		background-size: contain;
		background-repeat: no-repeat;
		background-position: center;	
		image-rendering: pixelated;
		image-rendering: crisp-edges;
	}

}

.itime {
	float: right;
	border: 1px solid #9a93b7;
	background: #fefefe;
	color: #050403;
	width: 128pt;
	padding: 2px;
	margin: 4px;
	display: inline-block;
	vertical-align: bottom;
	font-family: sans-serif;
	font-size: 10pt;
}

HASH(#itime-progress) {
	width: 0%;
	height: 100%;
	background: $404a68;
}

