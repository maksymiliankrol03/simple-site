#define HASH(x) x

#define C_PAGEBG #221f31
#define C_HEADERBG #221f31
#define C_BODYBG #fefefe
#define C_FOOTERBG #050403

#define C_HEADERFG #fefefe
#define C_BODYFG #050403
#define C_FOOTERFG #fefefe
#define C_BLEAKFG #9a93b7

#define C_BBORDER #221f31

#define C_DLINK #221f31
#define C_LINK #404a68
#define C_VISLINK #678fcb
#define C_FOOTLINK #8be1e0

#define C_DBORDER #404a68
#define C_LBORDER #9a93b7

#define C_DERR #a14d3f
#define C_LERR #ea9182
#define C_DINF #221f31
#define C_LINF #8be1e0
#define C_DWRN #9b6e2d
#define C_LWRN #f5ee9b

#define C_LGREEN #7cc264
#define C_DGREEN #316F23

#define C_NAVFG #fefefe

blockquote {
	margin: 6pt;
}

.header {
	color: C_LINF;
}

.header h2, .header h3 {
	display: inline;
}

.footer, .navbar {
	text-align: center;
}

.nav_linkmenu a {
	font-weight: bold;
	color: C_NAVFG;
}

.nav_linkmenu a:hover, .nav_linkmenu a:focus{
	background: C_DRED;
}

.nav_linkmenu a:focus {
	text-decoration: underline 2px;
}

.nav_linkmenu a:active {
	background: C_DGREEN;
}

.footer a, .footer a:visited {
	display: inline-block;
	color: C_FOOTLINK;
}

.content img {
	max-width: 100%;
}

.info, .warning, .error {
	margin: 6pt;
	padding-inline-start: 0.5em;
	font-family: sans-serif;
}

.info {
	background: C_LINF;
	color: C_DINF;
}

.warning {
	background: C_LWRN;
	color: C_DWRN;
}

.error {
	background: C_LERR;
	color: C_DERR;
}

.itime {
	display: none;
}

.def {
	text-decoration: dotted underline;
}
