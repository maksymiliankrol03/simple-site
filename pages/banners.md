% my website's banners
% /dev/urandom
% november 2020

# Banners

If you feel like it, you can put up these 88x31 banners linking to my website:

## rnd's corner

[![rnd's
corner](https://devurandom.xyz/banners/my_banner.gif)](https://devurandom.xyz)

HTML code:
```
<a href="https://devurandom.xyz"><img src="https://devurandom.xyz/banners/my_banner.gif" alt="rnd's
corner"></a>
```
## lipu sona pona

[![lipu sona
pona](https://devurandom.xyz/banners/tokipona_banner.gif)](https://devurandom.xyz/tokipona)

HTML code:
```
<a href="https://devurandom.xyz/tokipona"><img src="https://devurandom.xyz/banners/tokipona_banner.gif" alt="toki pona" title="learn a minimalist language of 120 words with 'lipu sona pona'></a>
```
