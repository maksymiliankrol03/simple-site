% rnd's links collection
% /dev/urandom
%now

This is a random collection of links to stuff I found interesting or useful.

## linux

 * [How to disable/enable journaling](https://foxutech.com/how-to-disable-enable-journaling) on an ext4 filesystem
 * ["Why I love Vim"](https://www.freecodecamp.org/news/learn-linux-vim-basic-features-19134461ab85/)
 * [How to enter arbitrary Unicode chars in vim](https://unix.stackexchange.com/questions/61118/how-to-enter-non-ascii-characters-using-hex-or-octal-codes-in-vi)
 * [How to do multi-hop SCP transfers](https://serverfault.com/questions/37629/how-do-i-do-multihop-scp-transfers)
 * [Changing the framebuffer terminal colors](https://askubuntu.com/questions/147462/how-can-i-change-the-tty-colors)
 * [Burning an ISO file from the terminal](https://askubuntu.com/questions/174626/how-do-i-burn-a-dvd-iso-using-the-terminal)
 * [Alpine Linux Syslinux manual](https://wiki.alpinelinux.org/wiki/Bootloaders)
 * [Automatic reboot after a kernel panic](https://www.cloudibee.com/automatic-reboot-kernel-panic/)
 * [Turning off buffering in pipes](https://unix.stackexchange.com/questions/25372/turn-off-buffering-in-pipe)

## coding
 
 * [Writing C software without stdlib](https://web.archive.org/web/20180423015837/weeb.ddns.net/0/programming/c_without_standard_library_linux.txt) ([alternate link](https://archive.md/u9Wrc))
 * [Common predefined OS-dependent macros](https://sourceforge.net/p/predef/wiki/OperatingSystems/)
 * [A tutorial on portable makefiles](https://nullprogram.com/blog/2017/08/20/) (i.e. not dependent on gnu make)
 * [nanoprintf](https://github.com/charlesnicholson/nanoprintf), an embeddable printf written in C99
 * [How Old School C Programmers Process Arguments](http://www.usrsb.in/How-Old-School-C-Programmers-Process-Arguments.html)
 * [awesome-c on Github](https://github.com/aleksandar-todorovic/awesome-c)
 * [pico-8 api cheatsheet](https://neko250.github.io/pico8-api/), somewhat out-of-date
 * [JSON-C library docs](https://json-c.github.io/json-c/json-c-0.13.1/doc/html/index.html)
 * [Implementing SSL on 8-bit micros](https://www.embedded.com/implementing-ssl-on-8-bit-micros/)	
 * [Sega Genesis/MD VDP general usage](https://segaretro.org/Sega_Mega_Drive/VDP_general_usage)
 * [x86/AMD64 instruction reference](https://www.felixcloutier.com/x86/index.html)
 * [Sega Genesis/Mega Drive VDP Graphics Guide v1.2a (03/14/17)](https://megacatstudios.com/blogs/retro-development/sega-genesis-mega-drive-vdp-graphics-guide-v1-2a-03-14-17)
 * [Linked lists, pointer tricks and good taste](https://github.com/mkirchner/linked-list-good-taste)

## gaming

 * [Phantasy Star Online | Dreamcast Online Multiplayer | Live Stream | 6/9/2018 - YouTube](https://www.youtube.com/watch?app=desktop&v=WEJ0KOC5U3c)
 * [Wave Function Collapse](https://marian42.itch.io/wfc) -- a game where you explore a procedurally generated city
 * [SIGIL](https://www.romerogames.ie/si6il/), a DOOM WAD made by John Romero
 * [Description of the IPS patch file format](https://zerosoft.zophar.net/ips.php)
 * [Rules for 3 player Riichi Mahjong](https://corp.mahjongclub.com/3-player-riichi)
 * [Panel Flux, a web puzzle game with procedurally generated puzzles](https://entanma.itch.io/panelflux)
 * [Ocarina of Time randomizer-inspired sudoku](https://bedibug.com/sudoku/#/)

## politics

 * [Jeremy Vine: My Boris (Johnson) Story](https://reaction.life/jeremy-vine-my-boris-story/)
 * [Yanks to the Rescue](https://archive.org/details/M.KramerYanksToTheRescue1996): how the Boris Yeltsin 1996 campaign hired American campaign advisors and used their techniques to win Russia's first democratic election

## history

 * [The Spartans Were Morons](https://taskandpurpose.com/the-spartans-were-morons)
 * ["Why isn't the new year on winter solstice?"](https://elekk.xyz/@noelle/99213354254637980): a Mastodon thread

## misc

 * [Pixel art tutorials](https://blog.studiominiboss.com/pixelart) by Studio MiniBoss
 * [Shuto Highway 83, a shader toy](https://www.shadertoy.com/view/XdyyDV)
 * [LRR's Checkpoint PUBG name supercut](https://awoo.space/@sc/99556552862600400)
 * [A beginner's guide to music theory](https://www.youtube.com/watch?v=n2z02J4fJwg)

## cool youtube channels

 * [Console Wars](https://www.youtube.com/channel/UC4Cza8wE3HGa4tq3WuwmbKw),
   cool dudes who compare games. Reminds me of the earlier days of YouTube.
 * [decino](https://www.youtube.com/channel/UCJ8V9aiz50m6NVn0ix5v8RQ), a DOOM
   let's-player also known for making videos on Classic DOOM's mechanics.
 * [LoadingReadyRun](https://www.youtube.com/channel/UCwjN2uVdL9A0i3gaIHKFzuA),
   Canadian comedians whomst did lots of sketch comedy and still do cool stuff
   like CheckPoint, Friday Nights and QWERPLINE
 * [SNES drunk](https://www.youtube.com/channel/UCfBLXTwLoUpDAkHcHizW3Jg), does
   videos on both famous and obscure SNES, Genesis and arcade games
 * [Jay Foreman](https://www.youtube.com/channel/UCbbQalJ4OaC0oQ0AqRaOJ9g),
   British funny dude who does "Map Men", "Unfinished London" and "Politics
   Unboringed".
 * [Secret Base](https://www.youtube.com/channel/UCDRmGMSgrtZkOsh_NQl4_xw),
   awesome sports nerds
 * [Overly Sarcastic
   Productions](https://www.youtube.com/channel/UCodbH5mUeF-m_BsNueRDjcw), make
   good videos about history and mythology
 * [Stop Skeletons From
   Fighting](https://www.youtube.com/channel/UC5Xeb9-FhZXgvw340n7PsCQ), videos
   on cool and obscure games and peripherals
 * [Jim Sterling](https://www.youtube.com/channel/UCWCw2Sd7RlYJ2yuNVHDWNOA),
   regular videos on the state of the games industry
 * [TOEI TOKUSATSU
   WORLD](https://www.youtube.com/channel/UC7pddu3yyzkzFEiXfQLex3w), release
   lots of old tokusatsu shows on YouTube for free. Some even have English
   subtitles.

---
[Back to top page](/index.html)
