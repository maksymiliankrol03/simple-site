% on international languages
% /dev/urandom
% 2021-04-19

This started off as a comment I wanted to write to an article, but eventually
grew into a thing of its own.

<!-- cut -->

From what I understand, the biggest reason why people aren't okay with using
English as a worldwide language typically has to do with the fact that people in
English-speaking nations learn it as their native language and speak it all
their lives, whereas people elsewhere need to learn it separately as their
second language.

Additionally, learning English as a second language can end up becoming easier
or harder depending on the languages one's already familiar with. Speakers of
Germanic or Romance languages, which already share lots of grammar and
vocabulary, will have an easier time than speakers of Arabic or Chinese
languages.

This results in an environment biased in favor of English-speaking nations and
nations with similar languages.

The goal of an international language is typically to be easy to learn and use
regardless of one's native language. That's why so many of the more famous ones
(Esperanto, Volapük, Novial, LFN) are criticized for their Eurocentrism: their
vocabulary and parts of their grammar come from European languages at a rate
much greater than that of how many people actually speak European languages.

With regards to that, toki pona does a decent (but not perfect) job. Out of the
123 words defined in the official book, 57 are derived from languages of
European origins (Dutch, English, Esperanto, Serbo-Croatian and Welsh). In
addition, 16 words are derived from Finnish, a language spoken in Europe but
with Uralic origins, and 16 are from Tok Pisin, a creole spoken in Papua New
Guinea whose vocabulary is derived in large part from English and other European
languages. Even then, the list of other languages is varied: Cantonese, Georgian
and Mandarin all contribute more than a few words.

The most popular words invented by the community seem even less Eurocentric:
"epiku" and "jami" come from English, but "soko" comes from Georgian, "lanpan"
from Greek, "tonsi" from Mandarin, "soto" from Swahili, "isipin" from Tagalog,
"jasima" from Turkish, "linluwi" and "teje" from Welsh.

* [ODS spreadsheet of "pu" toki pona words by source lang](/tokipona/language_sources.ods)
