% this website is self-hosted now!
% /dev/urandom
% 2021-06-04

So, yes, the website which previously was at <https://rnd.neocities.org/> has
now moved to a self-hosted Linode running nginx and a custom domain at
<https://devurandom.xyz/>.

The old website is **not** going to be deleted. [Link
rot](https://en.wikipedia.org/wiki/Link_rot) is a serious issue that makes
finding information on the internet a lot harder over time.

In addition, I have created an
[archive copy](https://devurandom.xyz/archive/2021-06-04.tar.gz) of the pre-move
version of the website, available for everyone to download. (It contains some
extra files that aren't added to the website's [source
tree](https://gitlab.com/dev_urandom/simple-site) for copyright purposes.)

The old website will probably not be updated any more, but this one definitely
will. 

---

I decided to move the website for several reasons. 

First of all, I already have a VPS I'm barely using, so it wouldn't cost me
extra to use it for my website as well. 

Secondly, neocities, while it is a very good platform, is a bit restrictive. It
always forces HTTPS upon clients, which, while a good security move, means that
older web browsers are unable to use the website. On a self-hosted server, I can
pick the security settings the way I want, and I have set them up to use HSTS
(web browsers which have visited the HTTPS version of the website before will
force it, while unsecured HTTP can still be used by older browsers). Thanks to
this, now a "Dreamcast version" of the website exists at
<http://devurandom.xyz/dc> -- with a design restricted to what is supported by
DreamPassport and PlanetWeb browsers.

And third, with self-hosting, I actually have more control over the data I store
on the website, and can test different ideas and server-side scripts in the
future.
