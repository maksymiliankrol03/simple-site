% transliteration of russian / транслитерация русского языка
% /dev/urandom
% 2021-04-03

I'm kinda annoyed at how the traditionally used transliteration of Russian is
not completely unambiguous. The best example is the letter "`щ`", which is
transliterated as "`shch`", which in turn can be read as "`шч`", a letter cluster
that actually happens in Russian words like "веснушчатый" and is pronounced
differently.

This is my alternative system (or two systems, to be precise) that are
completely unambiguous.

The first column in the table contains the original Russian letter, and the
second one -- its traditional transliteration(s). The third column shows my
fully ASCII-compatible transliteration of that letter, and the fourth -- a
Unicode-based one that uses diacritics.

The ASCII transliteration avoids ambiguity by only using the letters "`j`" and
"`h`" as modifier letters. "`j`" is always at the start of a letter and "`h`" is
always at the end.

The Unicode transliteration never uses modifier letters and always assigns one
Russian letter to one character.

---

Меня напрягает то, что в традиционной транслитерации русского языка существуют
неоднозначные преобразования. Например, буква "`щ`" пишется, как "`shch`", что в
свою очередь, можно прочитать как "`шч`" -- комбинацию букв, которая
присутствует в таких словах, как "веснушчатый" и произносится не как "щ".

Здесь я предлагаю две свои альтернативные системы, которые таких
неоднозначностей не имеют.

В первом столбце таблицы содержится буква русского алфавита, а во втором -- её
традиционная транслитерация. В третьем -- моя транслитерация, полностью
использующая символы ASCII, а в четвёртом -- транслитерация с диакритическими
символами из Unicode.

В транслитерации для ASCII неоднозначность отсутствует из-за того, что символы
"`j`" и "`h`" используются только для изменения других букв. Причём "`j`"
встречается только в начале буквы, а "`h`" -- только в конце.

В транслитерации для Unicode каждая буква русского алфавита преобразовывается в
один символ Unicode.

---

| letter / буква | trad./трад. | rnd (ASCII) | rnd (Unicode) |
|:--------------:|:-----------:|:-----------:|:-------------:|
| а              | a           | a           | a             |
| б              | b           | b           | b             |
| в              | v           | v           | v             |
| г              | g           | g           | g             |
| д              | d           | d           | d             |
| е              | e           | e           | e             |
| ё              | yo, e       | jo          | ö             |
| ж              | zh          | zh          | ž             |
| з              | z           | z           | z             |
| и              | i           | i           | i             |
| й              | j           | y           | ĭ             |
| к              | k           | k           | k             |
| л              | l           | l           | l             |
| м              | m           | m           | m             |
| н              | n           | n           | n             |
| о              | o           | o           | o             |
| п              | p           | p           | p             |
| р              | r           | r           | r             |
| с              | s           | s           | s             |
| т              | t           | t           | t             |
| у              | u           | u           | u             |
| ф              | f           | f           | f             |
| х              | kh, h       | kh          | h             |
| ц              | ts, c, cz   | c           | c             |
| ч              | ch          | ch          | č             |
| ш              | sh          | sh          | š             |
| щ              | shch        | s'h         | ş             |
| ъ              | "           | '           | '             |
| ы              | y           | ih          | ì             |
| ь              | '           | j'          | j             |
| э              | è, e        | eh          | è             |
| ю              | yu          | ju          | ü             |
| я              | ya          | ja          | ä             |

---

When using the ASCII transliteration, it's allowed to spell "ь" at the end of
words as "`j`" instead of "`j'`".

При использовании транслитерации для ASCII, букву "ь" в конце слова можно
писать, как "`j`", а не как "`j'`".

---

Universal Declaration of Human Rights, Article 1 / Всеобщая декларация прав
человека, статья 1:

 * **Кириллица:** Все люди рождаются свободными и равными в своём достоинстве и
   правах. Они наделены разумом и совестью и должны поступать в отношении друг
   друга в духе братства.

 * **rnd (ASCII):** Vse ljudi rozhdajutsja svobodnihmi i ravnihmi v svojom
   dostoinstve i pravah. Oni nadelenih razumom i sovestj'ju i dolzhnih postupatj
   v otnoshenii drug druga v dukhe bratstva.

 * **rnd (Unicode):** Vse lüdi roždaütsä svobodnìmi i ravnìmi v svoöm
   dostoinstve i pravah. Oni nadelenì razumom i sovestjü i dolžnì postupatj v
   otnošenii drug druga v duhe bratstva.
