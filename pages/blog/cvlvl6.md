% "cvlvl6 fanfic"
% /dev/urandom
% 2021-03-31

Trying to preserve an old page that I had a hard time finding online.

<!-- cut -->

Apparently, a long time ago, in 2008, a person named "pingosimon" composed a
silly cover of the level 6 theme from "Castlevania" for the NES, calling it
"Cv Lvl 6". In response, a bunch of other folks made their own versions.

 * [Archived page on bossies.org](https://web.archive.org/web/20130404045951/http://bossies.org/CVLVL6)

 * [MP3 file on bossies.org](http://bossies.org/streifig/cvlvl6/pingosimon_-_Castlevania6Song.mp3)

Covers (links to bossies.org, unless mentioned otherwise):

 * [Vegeroth's Quebecois cover, from a playlist on VK](https://vk.com/dendyforever?w=wall-12076000_6791)

 * [Scaredsim's French cover](http://bossies.org/streifig/cvlvl6/Scaredsim_-_Cv6Sim_en_francais.mp3)

 * ~~Riders' rap cover~~ I was unable to find a copy.

 * [Hydrasphere's cover, from a playlist on VK](https://vk.com/dendyforever?w=wall-12076000_6791)

 * [Russian cover by Streifig](http://bossies.org/streifig/cvlvl6/streifig_-_Awful_Russian_Singing.mp3)

 * [Russian cover of the introduction theme](http://bossies.org/streifig/cvlvl6/streifig_-_cventree.mp3)

 * [Hat's reggae-style cover](http://bossies.org/streifig/cvlvl6/Hat_-_Pingovania.mp3)

 * [ansgaros' Finnish metal cover](http://bossies.org/streifig/cvlvl6/ansgaros_-_Castlevania_level_6_fanfic_finnish.mp3)

 * [Spamtron's industrial cover](http://bossies.org/streifig/cvlvl6/Spamtron_-_cvlvl6_fanfic_industrial.mp3)

 * [Cacomistle's "Cacovania"](http://bossies.org/streifig/cvlvl6/cacomistle_-_Cacovania.mp3)
 
 * [Cacomistle's "Crunchlevania"](http://bossies.org/streifig/cvlvl6/cacomistle_-_Crunchlevania.mp3)

 * [Eric Dude's whistle mix](http://bossies.org/streifig/cvlvl6/Eric_Dude_-_Cvlvl6_Whisper_Mix.mp3)

 * [pIENESS' cover](http://bossies.org/streifig/cvlvl6/pIE_pIE_-_CV_LVL_6.mp3)

 * [zangderak and sparky's remix](http://bossies.org/images/f/fe/Sparkyzang-cvlvl6fanfic.mp3)

---

 * [Archive of everything I could find on MEGA](https://mega.nz/file/p44mHBCL#wyRuLYkNHKM2wnZchTw8ilt1ePgadWDcyXC4QP6ATl0)

