% cuda debugging notes
% /dev/urandom
% 2021-05-19

So, turns out, when you run a program that uses CUDA, either directly or
indirectly (my program runs on CUDA, but uses OpenCL instead), [there are
issues](https://github.com/google/sanitizers/issues/629) preventing `libasan`
from working. More specifically, if you run a program with `libasan`, the CUDA
libraries will not work, and the OpenCL ones will not display an NVIDIA platform
as available in the first place.

From my experience, seems like there are similar issues preventing Valgrind from
working, either.

--- 

Also, when a program is being run using `mpirun` or another such utility, the
environmental variable that shows the process's MPI rank is confusingly named
`$PMI_RANK`. This variable seems to be used by the Intel and Microsoft
implementations of MPI.

---
