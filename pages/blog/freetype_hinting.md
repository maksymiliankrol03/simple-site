% freetype hinting settings
% /dev/urandom
% 2021-02-26

I have a bunch of FreeType settings that I prefer on my systems, and these
differ from the defaults on basically every Linux distro these days.

<!-- cut -->

The most important one is the "interpreter version" setting. Several years ago,
the norm was version 35, corresponding to how Windows would typically render the
fonts. When used with full hinting, it resulted in typical desktop fonts, such
at those from the "DejaVu", "Liberation" families and "Droid Sans" looking very
crisp. It also worked well with Microsoft's "Core fonts for the Web" set, to the
point that if extra settings were added to remove antialiasing altogether on
these fonts, the end result was almost indistinguishable from pre-Cleartype
Windows operating systems.

Nowadays, the default is version 40, which does less hinting and resembles the
type of font rendering normally seen in Windows 10. While it looks better on
fonts that are not specifically designed for pixel-perfect hinting, and makes
the fonts look more like what they'd look printed, I dislike this option. So, in
`/etc/profile.d/`, I typically put a file named `freetype.sh` that looks like
this:

```
#!/bin/sh
export FREETYPE_PROPERTIES="truetype:interpreter-version=35"
```

With this setting enabled, it's also good to install the older 1.05.3 version of
the Liberation fonts. The later versions, built for the version 40 interpreter,
don't look as good on full hinting if version 35 is used. Seems like [this
page](https://releases.pagure.org/liberation-fonts/) provides copies of the
older versions of these fonts. (The versions 1.06 and 1.07 have some rather
annoying modifications, most notably in the lowercase letters "u" and "x" as far
as I recall.)

## Fonts that, IMO, look better with v35 hinting

* DejaVu Sans, Serif, Mono

* Liberation Sans, Serif, Mono (version 1.05)

* Ubuntu font family

* Droid Sans

* PT Mono, Sans, Serif
