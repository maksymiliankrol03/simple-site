% vim hints
% /dev/urandom
% 2021-04-07

These are some of the useful settings and functions I use when working with vim.

<!-- cut -->

## vimrc settings

```
if has("gui_running")
  
else
  set background=dark
endif
```

Make sure Vim uses the light colorscheme when in gvim, but the dark one when in
console mode.

`set colorcolumn=+1`: highlight the column after the maximum text width

```
set ignorecase
set smartcase
```

Only do case-sensitive searches if the search text has uppercase characters.

```
set laststatus=2
autocmd BufNewFile,BufRead *.md setlocal tw=80 nocindent statusline=%<%f\ %h%m%r[%{wordcount().words}\ words]%=%-14.(%l,%c%V%)\ %P
```

Whenever I'm editing a Markdown document, set the status line to display the
number of words in the document and set line width at 80 characters.

## commands

* `gqap`: format the current paragraph according to word-wrapping rules

* `"+y`: copy text specifically into the system's clipboard

* <code>magg=G`a</code>: indent the entire file, but return to the place where you started
