% toki pona designs for acnh
% /dev/urandom
% 2021-01-06

This is a page listing some of my ACNH posters for movies, games and such that
feature sitelen pona writing, along with what they're referring. When multiple
titles are given, the option written first is what the translation is taken from
(e.g. the Terranigma one is based on the game's Japanese title).

<!-- cut -->

![image 1](https://pbs.twimg.com/media/ErDMOnWUUAEEEyl?format=jpg&name=large)


---

 * toki pi ijo lon ala pi nanpa pini: musi nanpa 7 (Final Fantasy 7)
 * utala musi pi tomo tawa Tetona (Daytona USA)
 * luka kiwen pi mani mute (Бриллиантовая рука)
 * ijo ike tan mun ante (Alien)
 * open pi ma en sewi ("beginning of land and sky", 天地創造/Terranigma)
 * mama meli: musi #3 (Mother 3)
 * ilo jan pi ante selo ("robots of shape-changing", Transformers)
 * o tawa monsi tawa tenpo kama (Back to the Future)
 * wawa tawa (Speed)
 * nasin pi wile utala ("streets of fighting-desire", Streets of Rage)

---

 * o kama jo utala e tomo tawa ("steal a car", Grand Theft Auto)
 * ilo pi lipu nasin open (open-source hardware)
 * ilo kon pi lipu nasin open (open-source software)
 * utala pi mun seli (Star Wars)
 * jan li wile ala moli (Die Hard)
 * jan pi toki telo ([Canada] Moist Talkers)
 * wawa pi lipu kule ("power of petals", Petal Crash)
 * ante nasa lon tomo pi nasin wawa ("crazy changes in a courtroom",
逆転裁判/Ace Attorney)
 * <!-- --> 
 * <!-- -->

---

 * open ale pi kulupu Seka (Sega Genesis)
 * utala moli (Mortal Kombat)
 * mun pi kon pona (Lucky Star)
 * <!-- -->
 * jan pi utala nasin: musi nanpa tu (Street Fighter II)
 * <!-- -->
 * wawa pi kalama wawa sewi: musi nanpa tu tu (Thunder Force IV)
 * o alasa lon ma kasi soweli (あつまれ どうぶつの森/Animal Crossing: New
Horizons)
 * open ale pi kulupu Seka (Sega Genesis)
 * lon insa kulupu mi (Among Us)

---

 * mun seli pi lon ala: musi pi ilo sona weka (Phantasy Star Online)
 * sama akesi seli (龍が如く/Yakuza)
 * <!-- -->
 * kulupu sona pi kon mun ("knowledge-community of space")
 * ilo moli (The Lethal Weapons)
 * utala musi pi ko kule: musi nanpa tu (Splatoon 2)
 * toki suli pi jan Seta (The Legend of Zelda)
 * jan sama wawa Mawijo (Super Mario Bros.)
 * o kama jo e lipu nasin ("get an instruction book")
 * musi sama utala wawa (Super Smash Bros.)

---


![image 2](https://pbs.twimg.com/media/ErDMOnMUcAAljNk?format=jpg&name=large)


---

 * o kama pona! (Welcome!)
 * tomo tawa esun nasa (Crazy Taxi)
 * telo sijelo loje (Blood)
 * ijo pi leko tu tu li kama anpa (Tetris)
 * jan Nuken: musi nanpa tu wan (Duke Nukem 3D)
 * <!-- -->
 * <!-- -->
 * <!-- -->
 * tomo awen pi kiwen soweli (Wolfenstein 3D)
 * kama suli pi soweli Soni: musi nanpa tu (Sonic Adventure 2)

---

 * meli jaki tu (Dirty Pair)
 * open ale pi kulupu Seka (Sega Genesis)
 * <!-- -->
 * <!-- -->
 * <!-- -->
 * musi pi utala musi ni: "jan pi utala nasin: musi nanpa tu" (this
competition's game: Street Fighter II)
 * utala musi pi ma lon telo / lon tenpo ni a (island video game competition /
today!)
 * <!-- -->
 * <!-- -->
 * <!-- -->

---

 * ma tomo Makuwa (Moscow)
 * kama suli pi soweli Soni: musi nanpa tu (Sonic Adventure 2)
 * <!-- -->
 * <!-- -->
 * <!-- -->
 * sama akesi seli: musi nanpa ala (龍が如く０/Yakuza 0)
 * kama suli pi soweli Soni: musi nanpa tu (Sonic Adventure 2)
 * wawa suno loje Silijon (Red Photon Zillion)
 * ona li toki insa (it's thinking)
 * mun seli pi lon ala (Phantasy Star)

---

 * ijo ike mute ("many bad things", Doom)
 * o pali mute lon tenpo lili kepeken ilo ni. (work faster with this tool.)
 * meli jaki tu (Dirty Pair)
 * ilo musi lawa pi kulupu Seka (Sega Master System)
 * jan utala lon sewi weka mun (Space Harrier)
 * tawa poka lon open ("side-movement in the beginning", Initial D)
 * tomo tawa utala pi kiwen ilo ("metallic fighting vehicle", Metal Slug)
 * ijo jan li moli e jan: sitelen tawa nanpa tu (Terminator 2)
 * mun weka li pana e jan ike ("far-away planet is sending bad people", Contra)
 * tawa lon tomo tawa ("movement in a car", OutRun)

---



