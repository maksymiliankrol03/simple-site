% about me
% /dev/urandom
% july 2020

**name**: `/dev/urandom` (or `dev` for short, sometimes `rnd`, `jan Lentan` in
toki pona)

**date of birth**: june 1993

**gender**: no idea, think i might have lost it a few years ago

**job**: programmer

**tech**: i'm mostly a computing and retro software/gaming dork. i am kind of a
linux/open-source fan, although i try not to be a jerk about it as some do. i
occasionally code something in C or C++ in my personal time.

**politics**: i'm somewhere in the middle between progressive liberalism, social
democracy and libertarian socialism. i try to appreciate different people's
approaches to improving their societies, even if i disagree about the details.
having said that, i do not have any love for fascists, reactionaries, social
conservatives or bigots of any kind, as well as any viewpoint that values
ideology over actually solving problems.

**religion**: i am an agnostic/atheist, but i try to respect other people's
religious or spiritual beliefs (as long as these beliefs don't infringe on
other people's freedoms or aren't enforced on others politically or socially).

**favorite movies**: typically old comedies and silly action movies of the 80s
and 90s, but I also like an occasional modern one.

**favorite tv shows**: I don't watch a lot of TV really, but I occasionally
binge-watch some old shows if I find them interesting. I also occasionally enjoy
anime, but I wouldn't call myself an anime fan.

**favorite music**: at my home, the family usually tunes the radio to a
1970s/80s/90s oldies station. I enjoy some of that era's songs and tolerate
others. I also have a big digital collection of music from video games, and
recently I've been getting into synthwave music.

**favorite games**: when it comes to traditional games, I enjoy riichi mahjong
and have some familiarity with chess and shogi (I know the rules, but I'm pretty
bad at actually playing the games). I want to get into tabletop RPGs, but I
don't know how. I occasionally like playing board games with people online,
typically more simple ones. As for video games, I have a long list, but in
general I like a good platformer, some puzzle and fighting games and classic
Sega arcades.

* [more detailed list of my faves](my_faves.html)

<span id="contacts">**contacts**: </span>

 * email (phonetic alphabet): delta echo victor, period, uniform romeo alfa 
   november delta oscar mike, at sign, papa oscar sierra tango echo oscar, 
   period, oscar romeo golf

 * [Delta Chat](https://delta.chat/en/): same as the email address

 * [Twitter](https://twitter.com/_dev_urandom_)
 * [Mastodon](https://cybre.space/@devurandom)
 * [Gitlab](https://gitlab.com/dev_urandom)
 * [Ko-fi](https://ko-fi.com/dev_urandom)

