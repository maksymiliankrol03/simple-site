% my faves
% /dev/urandom
% march 2021

# movies

* The "Back to the Future" trilogy.

* "Die Hard" 1 and 2. Not Christmas movies, but good to watch during the holiday
  season anyway.

* "T2: Judgment Day". I should watch the first movie some day.

* The Ray Harryhausen Greek mythology movies: "Jason and the Argonauts" and
  "Clash of the Titans".

* "Last Action Hero". A silly and cheesy Schwarzenegger movie.

* "The Running Man".

* "Total Recall".

* "Scarface".

* "Escape from New York".

* "RoboCop". Should I watch the sequels?

* "Star Trek" movies, more specifically 2, 3 (a little bit), 4 and 6.

* "Speed".

* "Minority Report".

* "Taxi" 1--4 (the French series written by Luc Besson).

* "Galaxy Quest".

* "Van Helsing" (2004). With its mish-mash of different classical horror
  monsters and a plot centered around a vampire killer out to end Dracula, it's
  basically the movie version of the old Castlevania games.

* "Street Fighter". An absolutely cheesy movie with an amazing performance by
  Raúl Juliá.

* "Tropic Thunder". Not a fan of the "full r-word" scene, though.

# tv shows

* "Star Trek". The original series is cheesy and IMO doesn't hold up as well,
  but it's okay. TNG is awesome, and right now I'm in progress of watching DS9,
  which so far also seems cool.

* "The Office". I can see why this show spawned so many internet memes and
  became so famous. Tried watching "Parks and Rec", but I didn't like its tone
  as much.

* "Cobra Kai". Apart from a few jokes at the expense of "political correctness"
  (which are probably justifiable given the series' plot), this is an excellent
  series that actually makes me want to watch the "Karate Kid" movies as well.

* "Norsemen". A Norwegian comedy series about vikings which juxtaposes the
  brutal life and activity of Viking Age society with the more relaxed attitudes
  of modern-day Scandinavians.

* "Game Center CX". A Japanese series in which middle-aged comedian ARINO Shinya
  plays through old video games. [SA-GCCX](http://www.sa-gccx.com/) and
  [GooseCanyon](https://twitter.com/canyongoose) do good subtitled translations
  of the show's episodes, and some have also been officially translated and
  released in English under the title "Retro Game Master".

* "Dirty Pair". A silly anime from 1985 about two mercenary girls who go after
  dangerous people and cause lots of collateral damage. Very enjoyable.

* "Red Photon Zillion". A story about humans fighting off a genocidal alien
  force using special guns. Ends with an interesting twist. Was made in
  collaboration with Sega, and it shows: Opa-Opa from *Fantasy Zone* is the
  comic relief character, and at one point the main character is actually
  playing a Sega Master System. I watched through it all and I kinda liked it.

* "Initial D". A racing series famous for making eurobeat music and the Toyota
  AE86 a legendary vehicle not just in Japan, but across the world. I watched
  some of its First Stage, but lost track.

* "Lucky Star". Seems like it was made for people way geekier than me. I only
  understood a handful of references.

* "Death Note". Over here, it used to be considered really edgy back when it was
  made? Like, apparently parents groups got really mad, and even in 2021, a
  Russian court banned a couple of pages about it.

* "Area 88". A manga about an airline pilot forced into fighting a civil war in
  the Middle East as a mercenary. I watched the OVA, and it seemed pretty cool.
  There's also a video game adaptation, known in the West as "UN Squadron".

* "Akagi". A manga and anime series about a mahjong player who loves nothing
  more than a good gamble, and uses his skills, luck and an occasional trick to
  defeat a heartless millionaire. I watched the anime, and I *kinda* wanna read
  the manga, but I read that it's *really* long and slow.

# music

At home, my family really loves listening to a 70s/80s/90s oldies radio station
(but one that recently seems to have specifically started playing songs used in
memes, like "Africa" by Toto and "Never Gonna Give You Up" by Rick Astley). I
like some of the songs, but barely tolerate others.

Here are some I actually enjoy listening to:

* A Flock of Seagulls - I Ran

* Bill Conti - Gonna Fly Now (aka "the Rocky theme")

* Cheap Trick - Mighty Wings (aka "that song Yoko Shimomura ripped off to make
  Ken's Theme from Street Fighter II")

* Danzig - Mother

* Dschinghis Khan - Moskau

* Dead or Alive - You Spin Me Around (I actually found out about this song from
  a YouTube channel that played MSX Metal Gear games).

* Europe - Final Countdown

* Frankie Goes to Hollywood - Two Tribes (think I learned about it from Vice
  City)

* Gary Numan - Cars

* Guns 'N Roses - Sweet Child o' Mine, Welcome to the Jungle, Paradise City

* Haddaway - What is Love

* Huey Lewis & the News - Power of Love

* Iron Maiden - 2 Minutes to Midnight

* Kenny Loggins - Danger Zone

* Laura Branigan - Self Control

* Dick Dale and His Del-Tones - Misirlou

* Nena - 99 Luftballoons

* Paul Engemann - Push It to the Limit

* Scorpions - Wind of Change

* The Eagles - Hotel California

I have a large collection of music taken from video games, especially NES,
Genesis/Mega Drive and Super NES games in their respective .nsf, .vgm and .spc
formats. 

Also, I've recently gained some appreciation for the synthwave genre, and the
following composers in particular:

* A.L.I.S.O.N

* Carpenter Brut

* Dan Terminus

* Magic Sword

* Mitch Murder

* The Midnight

* Waveshaper

Also, the parody metal band "Nanowar of Steel".

# games (non-video)

* riichi mahjong.

* I have some appreciation for chess and shogi, but I don't have the tactical
  mind to actually play them.

* "Secret Hitler": a hidden-role game where liberals are tasked with stopping
  the rise of fascism in 1930s Germany.

* "Codenames": a word game where two teams have to guess their words based on
  vague one-word clues while avoiding the other team's words or black "assassin"
  words.

# games (video)

* Super Mario Bros. 1, 2, 3, World and Odyssey. 

* Sonic the Hedgehog 1, 2, 3&K, sometimes CD and 3D Blast, Adventure 1 and 2, most of
Generations and Mania.

* Final Fight (arcade) and Streets of Rage 1, 2 and 4.

* Street Fighter II in all of its variations (tho I'm not a competitive player).

* Neo Turf Masters.

* Tetris (in both old and modern interpretations, though things like T-Spins are
  still hard for me).

* Puyo Puyo, though I'm even worse at it than at Tetris.

* Castlevania 1, 3 and Bloodlines.

* Contra and Super C.

* Chrono Trigger.

* Terranigma.

* Sim City 1 (including the SNES port), 2000, 3000 and a bit of 4.

* Cities:Skylines is good, but I play it rarely.

* The Punch-Out!! games on the NES and SNES (I sometimes want to get a Wii just
  to try out the Wii game).

* Most of the classic SEGA acade games (and their ports) of the 1980s (Outrun,
  Hang On, Fantasy Zone, Golden Axe, Space Harrier, After Burner...)

* Yakuza 0, Kiwami 1 and 2, and "Like a Dragon".

* Metal Gear Solid 1.

* Phantasy Star Online is cool, but I often forget about it.

* The Road Rash series.

* Used to play Overwatch, but stopped after the whole Hong Kong controversy. Did
  Blizzard apologize for that?

* Jet Set Radio. I should play Future someday.

* 3D era Grand Theft Auto games (3, Vice City, San Andreas, LCS, VCS): the HD
  ones (IV, V) are also good, but I play them more rarely.

* Crusader Kings II and III (I should really get back into them).

* All of the Zachtronics coding games (TIS-100, Shenzhen I/O, Exapunks), until I
  get stuck on something.
