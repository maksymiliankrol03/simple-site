% rnd's doom section
% /dev/urandom
% date unknown

Out of all PC games that have been around for decades, id Software's DOOM is
probably one with the largest longevity. People still make new levels for it,
write new engines and modifications for it, and do speedruns for it under a
variety of conditions, with new records still being broken every now and then.
For almost every piece of hardware which can output graphics and run code,
there have been attempts to port or clone DOOM, with "It runs Doom" becoming a
popular internet meme.

I'd say there are many reasons for such longevity: the fact that the code of the
game has been open-sourced and is since available for anyone to build upon; the
well-designed separation of content and engine that makes it possible to add new
levels, graphics and sounds just by applying an additional WAD; and last, but
certainly not least -- the fact that it's a well-designed game that vastly
improved on its predecessors and became a success that was very hard to properly
replicate.

# getting the game

Nowadays, getting yourself a copy of Doom is pretty easy. You can buy both DOOM
and its sequel (and the addons) on GOG or Steam:

* **"The Ultimate Doom"** is the most up-to-date version of the first Doom game,
  as released in 1993 and updated in 1995 with the addition of the fourth
  episode, "Thy Flesh Consumed". It's divided into 4 episodes of 8 levels (plus
  a secret level) each.

* * [Ultimate Doom on Steam](https://store.steampowered.com/app/2280/Ultimate_Doom/)
* * [Ultimate Doom on GOG](https://www.gog.com/game/the_ultimate_doom)

* **"Doom II: Hell on Earth"** is the 1994 sequel to Doom, which expands on the
  original with a set of 32 new maps, new enemies, a new weapon (the Super
  Shotgun) and new power-ups.

* * [Doom II on Steam](https://store.steampowered.com/app/2300/DOOM_II/)
* * [Doom II + Final Doom on GOG](https://www.gog.com/game/doom_ii_final_doom)

* **"Final Doom"** is a collection of two 32-level WADs, created by outside
  developers for id and released commercially. These include "TNT: Evilution"
  and "The Plutonia Experiment". Both are known for their increased difficulty
  (and especially Plutonia) over the original game.

* * [Final Doom on Steam](https://store.steampowered.com/app/2290/Final_DOOM/)

* **"Master Levels for Doom II"** is an even later set of 20 WADs (each with one
  level), also created by outside developers for id and released commercially.

* * [Master Levels on Steam](https://store.steampowered.com/app/9160/Master_Levels_for_Doom_II/)

All of these will include the games' WAD files, that you can copy and use with
any source port. The Steam versions of Doom and Doom II use a pretty okay source
port based on the Unity engine with some improvements (like widescreen support
and higher-resolution display), whereas all the other games and GOG versions
package the original games with DOSBox.

> %info%
>
> The versions that have the Unity engine also include a separate WAD file that
> adds new widescreen graphics to all officially-released games. In addition,
> there are edited graphics and sounds for the secret levels in "Doom II"
> designed to be more appropriate for the game's distribution in Germany.

Either way, they all include the WAD files, which can then be easily used in any
other source port of your choosing.

If you don't want to buy the original game, you can instead check out
[Freedoom](https://freedoom.github.io/): an open-source from-scratch recreation
of Doom's assets with all-new levels, graphics and sounds. "Freedoom: Phase 1"
fills the role of (and is compatible with) Ultimate Doom, and "Freedoom: Phase
2" substitutes for "Doom II" and "Final Doom". 

# source ports

However, the best play to way classic Doom is by using one of the many source
ports that exist for the game. These can provide extra features, more
customization, online multiplayer and support for different game mods.

* [Chocolate Doom](https://www.chocolate-doom.org) is the most basic option. It
  adds as little as possible and is most useful for people who want the most
  **vanilla**-like experience on a modern operating system.

* [Crispy Doom](https://www.chocolate-doom.org/wiki/index.php/Crispy_Doom) is a
  variant of Chocolate Doom that aims to be more user-friendly and nicer to the
  eye while still remaining 100% gameplay compatible. It doubles the game's
  screen resolution, adds a widescreen option, changes the default controls to
  the more commonly used `WASD` setup and adds support for "**limit-removing**"
  WADs (ones that, while not using any custom features, may require more than
  the vanilla Doom engine can handle).

* [Doom Retro](https://www.doomretro.com/) is another port based on Chocolate
  Doom, but this one adds lots of extra visual features. Instead of having a GUI
  where these features can be set, it uses a Quake-like pop-out console where
  commands can be entered and variables can be set. It also introduces a number
  of bugfixes and improvements to the default game -- but at the cost of being
  unable to make or play "demo" recordings from the original games.

* [PrBoom+](http://prboom-plus.sourceforge.net/) is a more extensive source port
  derived from the "**Boom**" family. It adds widescreen support, an in-game menu
  where all the extra settings can be edited, an optional fully-3D OpenGL
  renderer and other interesting features. In addition, since the "Boom" source
  port added lots of extra features for map creators, PrBoom+ also supports
  them. However, it also tries to preserve compatibility with the original game
  whenever possible.

* [GZDoom](https://www.zdoom.org/downloads) is the most powerful of all Doom
  source ports. Not only does it introduce such features as widescreen, 3D
  rendering, mouselook, jumping, crouching, a new physics engine, and tons more,
  it also lets mod makers customize the game itself in a wide variety of ways.
  All the cool mods that implement tons of new weapons or monsters or turn Doom
  into a completely different kind of game are typically made with **GZDoom** in
  mind.

* [Odamex](https://odamex.net/) is a multiplayer-oriented source port that's
  perfect for running traditional-style deathmatch and cooperative matches. It's
  based on an earlier version of ZDoom and is more traditional in its design,
  but still offers many of the extended features from Boom and ZDoom.

* [Zandronum](https://zandronum.com/) is the more extensive multiplayer source
  port. It is derived from a relatively recent (but still a bit old) version of
  GZDoom and offers many of its features, and is able to play many of the mods
  designed for GZDoom.

* [Doom Legacy](http://doomlegacy.sourceforge.net/) is a source port with an
  interesting, well, legacy. While still actively developed, it's not as popular
  as it was before. But it has served as the base for a popular total
  conversion, "Sonic Robo Blast 2", which turns Doom into a unique *Sonic the
  Hedgehog* fan-game.

# good wads

* **Vanilla**: Back to Saturn X [Episode
  1](https://www.doomworld.com/idgames/levels/doom2/megawads/btsx_e1) and
  [Episode 2](https://www.doomworld.com/idgames/levels/doom2/megawads/btsx_e2)

* **Limit removing**: [SIGIL](https://romero.com/sigil), a fifth episode for Doom 1
  designed by John Romero, one of Doom's original creators

* **Boom**: [Ancient
  Aliens](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/aaliens)

* **Boom**: [Stardate
  20X6](https://www.doomworld.com/files/file/17221-stardate-20x6/)

# my own wads

* [**Doom II De-Sandified**](/doom/desandy.zip): a shitpost of a WAD that
  replaces all Doom II maps designed by Sandy Petersen with their equivalents
  from the [Freedoom](https://freedoom.github.io/) project.

# my contributions

* [Colorblind-friendly keys and door indicators for Freedoom](https://github.com/freedoom/freedoom/pull/755)

# good videos

* [SO YOU WANT TO PLAY SOME DOOM](https://www.youtube.com/watch?v=ietb4JwaaXA),
  a good introductory video with a lot of emphasis on extensive mods that GZDoom
  can provide

* [A Subjective Look at Doom Source
  Ports](https://www.youtube.com/watch?v=HzauoLqk_9U) by Dwars

* [Getting Started With PrBoom+](https://www.youtube.com/watch?v=9scf1ISvYZs) by
  Dwars

