% license for this website
% /dev/urandom
% 2021-05-13

Most of this website's contents is available under the [MIT
license](https://opensource.org/licenses/MIT). Please check the [source code
page](https://gitlab.com/dev_urandom/simple-site) for more information.

In addition, the [toki pona course](tokipona/) is available
under the terms of the CC-BY-SA
[3.0](https://creativecommons.org/licenses/by-sa/3.0/) and
[4.0](https://creativecommons.org/licenses/by-sa/4.0/) licenses.

> %warning%
> The [Polish translation](tokipona/pl_index.html) is instead available under the terms of the [CC-BY-SA
> 3.0 Poland](https://creativecommons.org/licenses/by-sa/3.0/pl/) and
> [4.0](https://creativecommons.org/licenses/by-sa/4.0/) licenses.

If you want to use the contents of this website somewhere else in a way that
isn't covered by one of those licenses, don't hesitate
to ask me using any of the ways mentioned [on my "about"
page](about_me.html#contacts). I will most likely respect your request if it's
for non-commercial purposes or if the end result is open-source.
