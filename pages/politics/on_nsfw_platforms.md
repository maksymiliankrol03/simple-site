% on NSFW content segregation
% /dev/urandom
% 2021-05-24

The "kink at pride" discourse (basic question: "Should LGBT pride events feature
people in costumes that, while not inherently pornographic, are still considered
inappropriate for children?") that was happening on social media made me think
about the overall situation with how modern societies treat "not safe for work"
or "sexual" content.

By which I mean, platforms where people can make money are usually either ones
where NSFW content is not allowed or strictly regulated, or ones where it's the
only thing that exists. Even on platforms where it's already possible to
restrict content to only 18+ viewers, pornographic or sexual content is not
welcomed.

And this is not even talking about how, for many people, even wholesome LGBT
expression is treated as more inappropriate or sexual than similar heterosexual
content -- even in places where homophobia is discouraged.

Same goes in the real world -- whereas there are a ton of spaces where you need
to be 18+, whenever sexuality enters the picture, everyone suddenly gets
shocked, and a lot of people get even more shocked when non-heterosexual
relationships are involved.

---

Basically, what I'm trying to say if: I hope for a world where sex work is
treated the same as any other kind of work that you need to be 18+ to do or to
be serviced. I want to have a world where you don't __need__ separate platforms
for NSFW streaming or video making, and where the distinction between "porn" and
"not porn" is not as strict -- especially since it seems that whenever platforms
do try and make a strict distinction, they always end up overestimating what
counts as "porn". I hope for a world where a streamer can, for example, decide
to stream themselves playing a video game while naked, and (with the appropriate
limits on who can view it) do it on the same platform as everyone else, without
instead being forced onto a porn-specific platform with a different community
and different expectations. I hope for a world where a person can be sexually
open without that also disqualifying their other qualities or expertise in other
fields. And I hope for a world where someone doing the same while not being
straight doesn't make them even more marginalized.
