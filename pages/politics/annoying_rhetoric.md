% annoying left-wing rhetoric
% /dev/urandom
% february 2021

As someone who can supports ideas anywhere from center-left liberalism to
outright anarcho-communism and despises the right, I still find some arguments
made by the left, and some behaviors common on the left (at least online) very
annoying.

## "deaths of capitalism"

One of these arguments is when people respond to claims of "communism killed
millions!", usually referring to people imprisoned or shot by authoritarian
socialist leaders like Joseph Stalin and Mao Zedong, by claiming every death
from malnutrition, lack of healthcare, or other failure as a "death caused by
capitalism".

On one hand, this is an understandable argument -- indeed, the inherent issues
of a capitalist system are responsible for these -- and there are also people on
the right who similarly over-exaggerate communism-caused deaths, down to
including Nazi soldiers killed by the Soviet Army in WW2 -- but just like real
law has a distinction between murder, manslaughter, self-defense and neglect,
maybe similarly someone dying from hunger in the U.S. is not the same
as people being executed by the state, or starved during the Holodomor while
Stalin's government rejected foreign aid and had a well-known distaste towards
Ukrainians as an ethnicity (though if I had to guess, I'd say that the Great
Famine in Ireland _would_ be a suitable capitalist counterpart, as it was both
exacerbated by greed and the English despising the Irish?)

And, even if you accept the most charitable interpretations that, say, the
Holodomor was not intentionally caused or worsened, this still leaves a
problem, in that if _your_ preferred socioeconomic system ever gains power, it
would only be fair if it also gets judged by the same criteria. When your
favorite ideology overthrows capitalism and takes control over a region, it will
probably have to spend a decade rebuilding all of local industry in order to
suit its needs.  Trade networks, especially with countries like China and India
and probably also with neighboring nations, will be severed, and your region
will need to quickly figure out how to start locally producing all the things
for which it previously relied on foreign imports. During that time, people will
at best live in poorer conditions than before, and at worst some will end up
starving. Hospitals will end up lacking critically needed supplies until these
can be produced once again. And, from the perspective of others, if this causes
people to die, this would not be much different from other "communist" (or
"capitalist", really) deaths.

## "they took away my slaves"

A common joke I've seen on left-wing social media, including among people who
otherwise seem like they side with anarchism over authoritarian socialism, fits
into the pattern of "I hate the evil (Russian/Chinese/Cuban) communists because
they took my (parents/grandparents') (mansion/slaves/industrial monopoly)."

This joke, sadly, has an element of truth to it -- I did actually see a few
tweets from people who legitimately were outraged that a socialist revolution
would have issue with someone owning a huge mansion, or having slaves, or
controlling a large part of a nation's industry. But using these examples to
dismiss any complaints someone might have when an authoritarian government takes
away their property is wildly inappropriate given how the scale of historical
confiscations of property usually went way further than "people with mansions,
slaves and monopolies".

For example, the "dekulakization" process in the Soviet Union has led to 4
million people being arrested by the state:

> [V. Zemskov, 1995: on the scale of repressions in the
> USSR](http://ecsocman.hse.ru/data/066/890/1216/016Zemskov.pdf)
>
> I still consider the number of the "dekulakized" to be at 4 million people,
> which were divided into three groups who got the following sanctions: the 1st
> group was arrested and tried in court, the 2nd was evicted and sent to an
> exile settlement, the 3rd was evicted without being sent to an exile
> settlement.
>

According to the same V. Zemskov, 600 thousand of these people died during their
exile between 1930 and 1933.

In addition to that, the standards for who was determined to be a "kulak" were
very different across regions of the Soviet Union. These "local overreaches"
("перегибы на местах"), which were sometimes used to force peasants to join
collective farms, were later criticized by Stalin himself. Even after the
practice was officially stopped, individual regions continued to arrest and
exile people.

Regarding that, I feel like combining all victims of forced confiscation under
the stereotype is a rather harmful idea, especially when it's done by people who
otherwise recognize the USSR in general and Stalin's rule specifically as an
authoritarian state that betrayed the ideals of socialism.

I personally think that, if the end goal is indeed to abolish the notion of
private property, it would, in fact, be necessary to deprive some people of
their possessions. But this must be done in a smart and consistent manner, where
the people who lose their belongings because of it are truly those who deserve
it. While it's not unreasonable to believe that some people will resist against
their possessions being confiscated, and will fight that as they can, it's still
vitally important that these people be treated humanely -- it would come across
as extremely hypocritical if the people who would otherwise criticize the
state's legal monopoly on violence and decry all instances of police brutality
start approving of it once their approved group people has the right to said
violence.

---

[Back to main politics page](index.html)
