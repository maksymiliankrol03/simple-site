% on cops
% /dev/urandom
% 2021-08-27

Saw [this post](https://monads.online/@dankwraith/106823786310094138) on
Mastodon:

> when people say "what if the capitalists replace the police with a private
> army" like... what do you think the police are. you think those guys answer to
> you?

And, at first, I got kinda annoyed: of course law enforcement as it currently
exist does more than just function as the super-rich class's private army. For
one, it does occasionally solve crimes that hurt regular people as well! I don't
really *like* it when radical leftists remove all nuance from an issue!

But then, I stumbled upon a thought: the fact that law enforcement _does_
occasionally help people outside of the mega-wealthy can easily function as a
very useful PR tactic, specifically so that privileged and/or middle-class
people see law enforcement as more than just an occupying force.

[Cops aren't good at solving
crimes](https://www.huffpost.com/entry/the-police-arent-good-at-solving-crimes_n_5ee7b4fbc5b614b68adec9b1).
Sometimes, [they take more of people's property via civil asset forfeiture than
all burglaries
combined](https://www.washingtonpost.com/news/wonk/wp/2015/11/23/cops-took-more-stuff-from-people-than-burglars-did-last-year/).
If I get my laptop stolen, would $500 in returned value I get from it cover all
the damage law enforcement causes with every incident of police brutality or
from enforcing awful laws (or from not enforcing good laws)?

If there were, indeed, a situation in which a city had its police department
replaced with a private force that _only_ enforced some megacorporation(s)'
property rights, it would quickly lose legitimacy in the eyes of the people. I
would guess that a private police force would _want_ to occasionally solve a
regular-people crime just so that people don't see it as only there for the
rich, even if 90% of what it does still benefits the rich.

---

Another argument I used to find annoying is comparing law enforcement (or
national armies) to terrorist organizations. Saying that your local department
is in any way like ISIS sounds extreme, and superficially they are very
different.

But also, just because cops don't film videos out of caves where they demand to
have a fundamentalist religious state or don't upload footage of public
beheadings online doesn't mean they don't also use their violence to strike
terror into people's minds and impact political decisions.

Politicians who tried to reduce law enforcement's influence over their cities
and regions can quickly meet legal resistance from police unions. If a city's
budget happens to be largely financed by fines and penalties, then cops can
threaten to stop doing their job and thus hurt the city's ability to fund other
things. And let's not forget the fact that for the poor and marginalized, law
enforcement interactions are very likely to end badly and are theferore
literally terrifying.

Cops may not literally be Al-Qaeda, but sometimes they sure sound a lot like the
yakuza (which might claim charity and helping locals as proof of their
benevolence, but they're still a criminal organization).
