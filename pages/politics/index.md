% politics / политика
% /dev/urandom
% 2021-01-31

This page will contain my more political content, usually about Russian
politics, but other countries' issues may be included as well.

* [My political positions](my_positions.html)

Pages
---

* [Putin's palace and the associated protests](putins_palace.html)

* [The kinds of left-wing arguments I find annoying](annoying_rhetoric.html)

* [Personal opinion on how it kinda sucks that online platforms are so often
   separated between "pornographic" and anti-NSFW](on_nsfw_platforms.html)

* [an unusually anarchist argument about cops](on_cops.html)

* [(WIP) Political positions that I feel conflicted
   about](conflicting_positions.html)

Suggested resources
---

> [![Новая Газета](/banners/novaya.gif)](https://novayagazeta.ru/) **Novaya
> Gazeta**: (Russian) A newspaper known for its investigative reporting
>
> [![Meduza](/banners/meduza.gif)](https://meduza.io/) **Meduza**: a news website,
> available in both English and Russian, covering news from Russia and beyond.
> Recently declared a "foreign agent" on basis of secret evidence, and after
> losing all of its advertisers, switched to asking for donations from readers.
>
> [![ОВД-Инфо](/banners/ovd-info.gif)](https://ovdinfo.org/) **OVD-Info**: (Russian) -
> monitors people being detained by police at protests and offers help
>
> [![The Moscow
> Times](/banners/moscowtimes.gif)](https://www.themoscowtimes.com) **The Moscow
> Times**: an independent English-language newspaper covering news in Moscow and
> other parts of Russia
>
> [![proekt.media](/banners/proekt.gif)](https://www.proekt.media/en/home/):
> **proekt.media**: an independent resource that specializes in in-depth reports
