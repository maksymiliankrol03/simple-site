% putin's palace
% /dev/urandom
% 2021-01-31

On January 19th, 2021, Alexey Navalny's **Anti-Corruption Foundation** (Фонд
борьбы с коррупцией, ФБК/FBK) has released a documentary called "[**Putin's
palace. History of world's largest bribe**][palacevideo]" on YouTube. The video,
along with its [associated website][palacesite], talks about a building on the
Idokopas Cape near Gelendzhik ([OSM link][palaceosm]), the construction of which
is estimated to have cost over **100 billion rubles** and involved people
tightly connected, personally and professionally, to Russian President
**Vladimir Putin**.

[palacevideo]: https://www.youtube.com/watch?v=ipAnwilMncI
[palacesite]: https://palace.navalny.com/
[palaceosm]: https://www.openstreetmap.org/?mlat=44.41917&mlon=38.20528&zoom=12#map=12/44.4192/38.2053

The video was released shortly after Navalny returned to Moscow (and was quickly
arrested by police) after recovering in the Charité hospital in Berlin from what
the German government has revealed to be a [**Novichok chemical agent
poisoning**][novichok] likely perpetrated by **FSB agents** (Bellingcat has
since [determined their identities][bellingcat1] and [linked them to three other
activist deaths][bellingcat2]).

[novichok]: https://www.bbc.com/news/world-europe-54002880
[bellingcat1]: https://www.bellingcat.com/news/uk-and-europe/2020/12/14/fsb-team-of-chemical-weapon-experts-implicated-in-alexey-navalny-novichok-poisoning/
[bellingcat2]: https://www.bellingcat.com/news/uk-and-europe/2021/01/27/navalny-poison-squad-implicated-in-murders-of-three-russian-activists/

Navalny himself has made [a video][navalnyvideo] and [a blog post (with a
transcript)][navalnypost] detailing how he managed to call one of the people
involved in the poisoning and managed to [social engineer][soceng] him into
**admitting his involvement** and revealing extra details.

[navalnyvideo]: https://www.youtube.com/watch?v=ibqiet6Bg38
[navalnypost]: https://navalny.com/p/6447/
[soceng]: https://en.wikipedia.org/wiki/Social_engineering_\(security\)

While the building has been documented and previously referred to as "Putin's
palace" back in 2011, it didn't attract enough attention at the moment. The
video has provided lots of new information about the building, specifically
about the people and organizations involved in its construction, maintenance and
protection, and using blueprints and old photos (some of these are compiled in
[this blog post][palacephotos] to make
detailed **3D reconstructions** of the interior as originally built (though, as
the video notes, in 2021, the building's interior is being rebuilt completely
due to mold caused by poor ventilation).

[palacephotos]:https://yurayakunin.livejournal.com/13285019.html

The video has since accumulated over **100 million views** on YouTube, and at
the end, called for protests to happen on 23rd of January in cities all over
Russia.  Said protests [were carried out][protests23] in 198 Russian cities,
were referred to as "largest in the Putin era" by journalists, with Reuters
citing **40000 participants in Moscow** alone, and other reporters claiming
thousands participating in other cities. These protests were met with aggressive
police action, with at **least 3695 people detained**, according to
[OVD-Info][ovd23].

[protests23]: https://meduza.io/en/feature/2021/01/25/january-23rd-in-photos
[ovd23]: https://ovdinfo.org/news/2021/01/23/akcii-svobodu-navalnomu-23-yanvarya-2021-goda-onlayn

Similar protests [also occurred on the 31st of January][protests31].  This time,
the police presence was way more overwhelming, with streets close to the Red
Square in Moscow being **shut down completely** and other attempts to disrupt
transportation in other cities. At the point of writing (Feb 1, 20:11 Moscow
time), OVD-Info has reported [**at least 5646 arrests**][ovd31].  Nevertheless,
the protests in major cities, such as Moscow and St. Petersburg, continued for
about the same amount of time. The participants adapted by improvising the route
of their movement, so that police wouldn't be able to blockade it.

[protests31]: https://meduza.io/en/live/2021/01/31/navalny-s-nationwide-protests-round-ii
[ovd31]: https://ovdinfo.org/news/2021/01/31/zaderzhaniya-na-akciyah-v-podderzhku-alekseya-navalnogo-31-yanvarya-2021-goda-onlayn

On the 2nd of February, Navalny was **tried in court** for his alleged violation
of a suspended sentence, as the coma caused by his poisoning prevented him from
completing the probation check-ins. This is especially egregious as the European
Court of Human Rights has previously **overturned** Navalny's conviction that
caused the suspended sentence to happen and forced Russia to pay compensation
for the unjust conviction. The court case ended with [a sentence of **3.5 years
of prison**][verdict] for Navalny, reduced to 2 years and 8 months due to time
spent in house arrest.  Even before the verdict was announced, major streets in
Moscow and St.  Petersburg [were blockaded][blockade] in expectation of protests
-- which [were announced][protests2] immediately after. OVD-Info reports [1408
arrests][ovd2] in 10 cities.

[verdict]: https://www.themoscowtimes.com/2021/02/02/poisoned-putin-critic-navalny-gets-almost-3-years-in-prison-colony-a72810
[blockade]: https://twitter.com/thebellio/status/1356657650491129858
[protests2]: https://meduza.io/en/news/2021/02/02/navalny-s-supporters-call-for-protests-near-red-square-following-verdict-in-moscow
[ovd2]: https://ovdinfo.org/news/2021/02/02/spisok-zaderzhannyh-v-svyazi-s-sudom-nad-alekseem-navalnym-2-fevralya-2021-goda


---

* [Back to politics main page](/politics)
